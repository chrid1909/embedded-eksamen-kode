# In this code the user will be able, to receive data from XCTU via XBEE coordinator 

from digi.xbee.devices import XBeeDevice

# TODO: Replace with the serial port where your local module is connected to.
PORT = "/dev/ttyUSB0"
# TODO: Replace with the baud rate of your local module.
BAUD_RATE = 9600

def data_received(xbee_message):
    sender = xbee_message.remote_device.get_64bit_addr()
    message = xbee_message.data.decode() 
    print("Data received: From %s >> %s" % (sender,message))

def main():
    print(" +-----------------------------------------+")
    print(" | Receive data from XBee in XCTU          |")
    print(" +-----------------------------------------+\n")

    # Create the XBee device
    device = XBeeDevice(PORT, BAUD_RATE)

    try:
        # Open the XBee device for communication
        device.open()

        # Empty the packets queue
        device.flush_queues()
       
        #device.add_data_received_callback(data_received)

        print("Waiting for data...\n")
        #input()

        while True:
            xbee_message = device.read_data()
            if xbee_message is not None:
                data_received(xbee_message)

    finally:
        if device is not None and device.is_open():
            device.close()


if __name__ == '__main__':
    main()
