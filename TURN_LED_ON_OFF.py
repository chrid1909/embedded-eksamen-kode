from digi.xbee.devices import XBeeDevice
from digi.xbee.devices import RemoteXBeeDevice
from digi.xbee.devices import XBee64BitAddress 


PORT = "/dev/ttyUSB0"
BAUD_RATE = 9600
REMOTE_NODE_ID = "0013A20041B89B35" # MAC address of Remote XBee (Coordinator)

# The End point xbee has received a message. 
# If the msg is Led On, the Led is turned on.
# If the msg is Led Off, the Led is turned off
def data_received(device, remote_device, xbee_message):
    sender = xbee_message.remote_device.get_64bit_addr()
    message = xbee_message.data.decode() 
    
    print("Data received: From '%s' >> '%s'" % (sender,message)) 
    if message == "LED ON":        
        turn_on_led(device, remote_device)
        
    if message == "LED OFF":
        turn_off_led(device, remote_device)

# Turns on the led and send response back to remote device (XCTU)
def turn_on_led(device,remote_device): 
    DATA_TO_SEND = "LED turned ON" 
    remote = remote_device.get_64bit_addr() 
    print("REMOTE: ", remote) 
    print("MESSAGE: ", DATA_TO_SEND) 
    device.send_data_async(remote_device, DATA_TO_SEND)
    print("MESSAGE SENT")  

# Turns off the led and send response back to remote device (XCTU)
def turn_off_led(device,remote_device): 
    DATA_TO_SEND = "LED turned OFF"  
    remote = remote_device.get_64bit_addr() 
    print("REMOTE: ", remote)     
    print("MESSAGE: ", DATA_TO_SEND) 
    device.send_data_async(remote_device, DATA_TO_SEND) 
    print("MESSAGE SENT")  
    

def main():
    print(" +-----------------------------------------+")
    print(" |          Turn LED ON and OFF            |")
    print(" +-----------------------------------------+\n")
 
    # Create the XBee device
    device = XBeeDevice(PORT, BAUD_RATE)

    # Obtain the remote XBee device from the XBee network.
    remote_device = RemoteXBeeDevice(device, XBee64BitAddress.from_hex_string(REMOTE_NODE_ID)) 
    if remote_device is None:
        print("Could not find the remote device")
        exit(1)
    
    try:
        # Open the XBee device for communication
        device.open()
          
          # Empty the packets queue
        device.flush_queues()

        while True:
            # Wait for data to arrive ...
            xbee_message = device.read_data()
            if xbee_message is not None:
                data_received(device, remote_device, xbee_message)

    except Exception as e:
        print ("Execption occured", e.message)
        

    finally:
        if device is not None and device.is_open():
            device.close()

if __name__ == '__main__':
    main()