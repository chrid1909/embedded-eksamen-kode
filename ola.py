from digi.xbee.devices import XBeeDevice 

# TODO: Replace with the serial port where your local module is connected to. 
PORT = "/dev/ttyUSB0" 

# TODO: Replace with the baud rate of your local module. 
BAUD_RATE = 9600 

# remote id address # remote node id, is the MAC address of the remote xbee device 
# 
REMOTE_NODE_ID = "End point" 

def turn_on_led(device,remote_device): 
    DATA_TO_SEND = "ON" 
    remote = remote_device.get_64bit_addr() 
    print("REMOTE: ", remote) 
    print("MESSAGE: ", DATA_TO_SEND) 
    device.send_data(remote_device, DATA_TO_SEND) 
    
def turn_off_led(device,remote_device): 
    DATA_TO_SEND = "OFF" 
    remote = remote_device.get_64bit_addr() 
    print("REMOTE: ", remote) 
    print("MESSAGE: ", DATA_TO_SEND) 
    device.send_data(remote_device, DATA_TO_SEND) 
    
def receive_data_from_remote_device(device,remote_device): 
    xbee_msg = device.read_data_from(remote_device) 
    if xbee_msg != None: 
        print("REMOTE: ", xbee_msg.remote_device.get_64bit_addr()) 
        print("MESSAGE: ", xbee_msg.data.decode('ascii')) 
        
def main(): 
    
    print(" +--------------------------------------+")
    print(" | XBee control turn on and of LED      |") 
    print(" +--------------------------------------+\n") 

    device = XBeeDevice(PORT, BAUD_RATE) 
    try: 
        device.open() 

        # Obtain the remote XBee device from the XBee network. 
        xbee_network = device.get_network() 
        remote_device = xbee_network.discover_device(REMOTE_NODE_ID) 
        if remote_device is None: 
            print("Could not find the remote device", REMOTE_NODE_ID) 
            exit(1) 
        
        # Here i tell the remote control to turn on 
        turn_on_led(device,remote_device) 
        
        # wait for led on reply from remote device 
        receive_data_from_remote_device(device,remote_device) 
        
        # Here i tell the remote control to turn off 
        turn_off_led(device,remote_device) 
        
        # wait for led off reply from remote device 
        receive_data_from_remote_device(device,remote_device) 

    finally: 
        if device is not None and device.is_open(): 
            device.close() 

if __name__ == '__main__': 
    main()
