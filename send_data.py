# With this code, the user will be able to send data from the raspberrypi to XCTU via XBEE's End point and Coordinator 

from digi.xbee.devices import XBeeDevice
from digi.xbee.devices import RemoteXBeeDevice
from digi.xbee.devices import XBee64BitAddress 


# TODO: Replace with the serial port where your local module is connected to.
PORT = "/dev/ttyUSB0"
# TODO: Replace with the baud rate of your local module.
BAUD_RATE = 9600

DATA_TO_SEND = "HELLO from End point!"
REMOTE_NODE_ID = "End point"


def main():
    print(" +--------------------------------------+")
    print(" | Send data from XBee to XCTU          |")
    print(" +--------------------------------------+\n")

    device = XBeeDevice(PORT, BAUD_RATE)

    try:   
        device.open()

        # Obtain the remote XBee device from the XBee network.
        xbee_network = device.get_network()
       # remote_device = xbee_network.discover_device(REMOTE_NODE_ID)
        remote_device = RemoteXBeeDevice(device, XBee64BitAddress.from_hex_string("0013A20041B89B35")) 
        if remote_device is None:
            print("Could not find the remote device")
            exit(1)

        print("Sending data to %s >> %s..." % (remote_device.get_64bit_addr(), DATA_TO_SEND))

        device.send_data(remote_device, DATA_TO_SEND)

        print("Success")

    finally:
        if device is not None and device.is_open():
            device.close()


if __name__ == '__main__':
    main()
